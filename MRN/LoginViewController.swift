//
//  LoginViewController.swift
//  MRN
//
//  Created by kawano on 2016/08/15.
//  Copyright © 2016年 kawano. All rights reserved.
//

import UIKit
import NCMB

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate = self
    }
    
    // Loginボタン押下時
    @IBAction func loginBtn(sender: UIButton) {
      login()
    }
    
    // SignUpボタン押下時
    @IBAction func signUpBtn(sender: UIButton) {
        // TextFieldを空にする
        cleanTextField()
        // errorLabelを空に
        cleanErrorLabel()
        // キーボードを閉じる
        closeKeyboad()
        
        //self.performSegueWithIdentifier("loginToSignUp", sender: self)
    }
    
    // 背景タップするとキーボードを隠す
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        //self.view.endEditing(true)
        closeKeyboad()
    }
    
    // Login処理
    func login() {
        // 入力チェック
        if self.userNameTextField.text!.isEmpty || self.passwordTextField.text!.isEmpty {
            self.errorLabel.text = "未入力の項目があります"
            return
        }
        
        // ユーザー名とパスワードでログイン
        NCMBUser.logInWithUsernameInBackground(self.userNameTextField.text, password: self.passwordTextField.text, block:{(user: NCMBUser?, error: NSError!) in
            // TextFieldを空に
            self.cleanTextField()
            if error != nil {
                // ログイン失敗時の処理
                self.errorLabel.text = "ログインに失敗しました:\(error.code)"
                print("ログインに失敗しました:\(error.code)")
            }else{
                // ログイン成功時の処理
                //self.performSegueWithIdentifier("login", sender: self)
                print("ログインに成功しました:\(user?.objectId)")
                let storyboard: UIStoryboard = self.storyboard!
                let searchView = storyboard.instantiateViewControllerWithIdentifier("search") as! SearchViewController
                self.presentViewController(searchView, animated: true, completion: nil)
            }
        })
    }

    // TextFieldを空にする
    func cleanTextField(){
        userNameTextField.text = ""
        passwordTextField.text = ""
    }
    
    // errorLabelを空にする
    func cleanErrorLabel(){
        errorLabel.text = ""
    }
    
    // キーボードを閉じる
    func closeKeyboad(){
        userNameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    // ReturnKeyが押された時の処理
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nextTag = textField.tag + 1
        if let nextTextField = self.view.viewWithTag(nextTag) {
            nextTextField.becomeFirstResponder()
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

