//
//  SignUpViewController.swift
//  MRN
//
//  Created by kawano on 2016/08/22.
//  Copyright © 2016年 kawano. All rights reserved.
//

import Foundation
import NCMB

class SignUpViewController: UIViewController, UITextFieldDelegate {
    // User Name
    @IBOutlet weak var userNameTextField: UITextField!
    // Password
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordTextField_second: UITextField!
    
    // errorLabel
    @IBOutlet weak var errorLabel: UILabel!
    
    
    // 画面表示時に実行される
    override func viewDidLoad() {
        super.viewDidLoad()
        // Passwordをセキュリティ入力に設定
        self.passwordTextField.secureTextEntry = true
        self.passwordTextField_second.secureTextEntry = true
        userNameTextField.delegate = self
    }
    
    
    // SignUpボタン押下時の距離
    @IBAction func signUpBtn(sender: UIButton) {
        // キーボードを閉じる
        closeKeyboad()
        
        // 入力確認
        if self.userNameTextField.text!.isEmpty || self.passwordTextField.text!.isEmpty || self.passwordTextField_second.text!.isEmpty {
            self.errorLabel.text = "未入力の項目があります"
            // TextFieldを空に
            self.cleanTextField()
            
            return
            
        } else if passwordTextField.text! != passwordTextField_second.text! {
            self.errorLabel.text = "Passwordが一致しません"
            // TextFieldを空に
            self.cleanTextField()
            
            return
            
        }
        
        //NCMBUserのインスタンスを作成
        let user = NCMBUser()
        //ユーザー名を設定
        user.userName = self.userNameTextField.text
        //パスワードを設定
        user.password = self.passwordTextField.text
        
        //会員の登録を行う
        user.signUpInBackgroundWithBlock{(error: NSError!) in
            // TextFieldを空に
            self.cleanTextField()
            
            if error != nil {
                // 新規登録失敗時の処理
                self.errorLabel.text = "ログインに失敗しました:\(error.code)"
                print("ログインに失敗しました:\(error.code)")
                
            } else {
                // 新規登録成功時の処理
                self.errorLabel.text = "ログインに成功しました:"
                print("ログインに成功しました:\(user.objectId)")
                let storyboard: UIStoryboard = self.storyboard!
                let searchView = storyboard.instantiateViewControllerWithIdentifier("search") as! SearchViewController
                self.presentViewController(searchView, animated: true, completion: nil)
            }
            
        }
        
    }
    
    // 背景タップするとキーボードを隠す
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        //self.view.endEditing(true)
        closeKeyboad()
    }
    
    // TextFieldを空にする
    func cleanTextField(){
        userNameTextField.text = ""
        passwordTextField.text = ""
        passwordTextField_second.text = ""
    }
    
    // errorLabelを空にする
    func cleanErrorLabel(){
        errorLabel.text = ""
    }
    
    // キーボードを閉じる
    func closeKeyboad(){
        userNameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordTextField_second.resignFirstResponder()
    }
    
    // ReturnKeyが押された時の処理
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let nextTag = textField.tag + 1
        if let nextTextField = self.view.viewWithTag(nextTag) {
            nextTextField.becomeFirstResponder()
        }
        return true
    }
    
}
