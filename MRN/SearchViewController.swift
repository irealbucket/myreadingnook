//
//  SignUpViewController.swift
//  MRN
//
//  Created by kawano on 2016/08/22.
//  Copyright © 2016年 kawano. All rights reserved.
//

import Foundation
import NCMB
import SwiftyJSON

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var Books: Array<BookDto> = []
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    func Search() {
        
        let searchText : String = searchBar.text!

        // 通信を行うためのNSURLSessionオブジェクトを取得
        let session = NSURLSession.sharedSession()
        let keyword = searchText.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        // URL(適当に変えて試してみて)
        let url = NSURL(string: "https://app.rakuten.co.jp/services/api/Kobo/EbookSearch/20131010?keyword=\(keyword)&applicationId=1010392497440859362")!
        
        // リクエスト
        let request = NSURLRequest(URL: url)
        
        // 非同期で実行される通信タスクを作成.        「{ 」以降は引数としてクロージャ（関数）を渡している(引数名:completionHandler)
        let task = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            //データ受信完了時の処理
            //if let data = data {
//                 デモなので文字列に変換(実際はJSONをパースしてオブジェクトに変換するなどしてください)
                //let value = String(data: data, encoding: NSUTF8StringEncoding) ?? "エンコーディング違う?"
                //print(value)
            //}
            let json = JSON(data: data!)
            let items = json["Items"]
            var books: Array<BookDto> = []
            
            for (_, subJson) in items {
                //print(subJson)
                let item = subJson["Item"]
                let title = item["title"].string
                let smallImageUrl = item["smallImageUrl"].string
                let authorKana = item["authorKana"].string
                print(item)
                print(title)
                
                let book = BookDto(title: title, smallImageUrl: smallImageUrl, authorKana: authorKana)
                
                books.append(book)
            }
            // メインスレッドにスイッチする
            dispatch_async(dispatch_get_main_queue(), {
                self.Books = books
                // テーブルビューを更新する
                self.table.reloadData()
            }) //in complitionHandler            self.Books = books
        }
        
        // タスク実行
        task.resume()
    }
    
    
    
    // 画面表示時に実行される
    override func viewDidLoad() {
        super.viewDidLoad()
        table.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        searchBar.delegate = self
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Books.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 
        cell.textLabel?.text = Books[indexPath.row].title
        cell.detailTextLabel?.text = Books[indexPath.row].authorKana
        return cell
    }
    
    //returnキーが押された時の処理
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        Search()
    }
    
    // 背景タップするとキーボードを隠す
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        //self.view.endEditing(true)
        closeKeyboad()
    }
    
    // キーボードを閉じる
    func closeKeyboad(){
        searchBar.resignFirstResponder()
    }
}
