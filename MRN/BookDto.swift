//
//  BookDto.swift
//  MRN
//
//  Created by kawano on 2016/08/31.
//  Copyright © 2016年 kawano. All rights reserved.
//

import Foundation

class BookDto {
    
    var title :String?
    var smallImageUrl :String?
    var authorKana :String?

    init(title: String?, smallImageUrl :String?, authorKana :String?){
        self.title = title
        self.smallImageUrl = smallImageUrl
        self.authorKana = authorKana
    }
}